new fullpage('#fullpage',{
    autoScrolling: true,
    scrollHorizontally: true,
    anchors: ['Welcome', 'About', 'Education', 'Awards', 'Skills', 'Others'],
    sectionsColor: ['#f2f2f2', '#4BBFC3', 'yellow', 'whitesmoke', '#f0f0f0']
});